# Bunsan Backend exam

Project to test abilities of developer in Golang.

It creates an HTTP server with 1 REST API, using a file, get digits, validate checksum and get status (OK, ERR, ILL).

## Run it locally
1. Clone project running `git clone https://gitlab.com/stratslab/bunsan-backend-exam.git` in your terminal.
2. Go to project's folder with `cd bunsan-backend-exam`.
3. Install dependencies running `go get`.
4. Run `go run main.go` in terminal.
5. Open Postman or any other HTTP client and make this request (POST): `http://localhost:8001/process-file`.

Example screenshot:

<img src="./files/screenshot.png" height="500" />

Examples (Request body must use multipart/form-data and "file" as key of the file):
* [Wrong extension](./files/dog.jpeg), Server status: 400 (Bad request), Message: "only .txt files are allowed".
* [Wrong characters count](./files/entries_3.txt), Server status: 400 (Bad request), Message: "Each entry must have at least 81 characters.".
* [1 element](./files/entries_2.txt), Server status: 200 (OK), Message: "".
```
[
    {
        "Digits": "111111111",
        "Status": "ERR"
    }
]
```
* [Data from PDF](./files/entries.txt), Server status: 200 (OK), Message: "".
```
[
    {
        "Digits": "000000000",
        "Status": "OK"
    },
    {
        "Digits": "111111111",
        "Status": "ERR"
    },
    {
        "Digits": "222222222",
        "Status": "ERR"
    },
    {
        "Digits": "333333333",
        "Status": "ERR"
    },
    {
        "Digits": "444444444",
        "Status": "ERR"
    },
    {
        "Digits": "555555555",
        "Status": "ERR"
    },
    {
        "Digits": "666666666",
        "Status": "ERR"
    },
    {
        "Digits": "777777777",
        "Status": "ERR"
    },
    {
        "Digits": "888888888",
        "Status": "ERR"
    },
    {
        "Digits": "999999999",
        "Status": "ERR"
    },
    {
        "Digits": "123456789",
        "Status": "OK"
    },
    {
        "Digits": "000000051",
        "Status": "OK"
    },
    {
        "Digits": "49006771?",
        "Status": "ILL"
    },
    {
        "Digits": "123??678?",
        "Status": "ILL"
    }
]
```

## Goals
* ✅ Get digits from file.
* ✅ Validate checksum.
* ✅ Get status depending of digits and checksum.

## Tech stack
* Go 1.18.
* MacOS Monterey 12.6 (Development).
