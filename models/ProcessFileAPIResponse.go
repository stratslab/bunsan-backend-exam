package models

// ProcessFileAPIResponse is used to represent an item of "/process-file" response.
type ProcessFileAPIResponse struct {
	Digits string
	Status string
}
