package main

import "gitlab.com/stratslab/bunsan-backend-exam/http-server"

func main() {
	http_server.RunServer()
}
