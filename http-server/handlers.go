package http_server

import (
	"bytes"
	"encoding/json"
	"errors"
	"gitlab.com/stratslab/bunsan-backend-exam/models"
	"gitlab.com/stratslab/bunsan-backend-exam/utils"
	"io"
	"math"
	"net/http"
	"strconv"
	"strings"
)

// ProcessFileHandler handles "/process-file" request.
func ProcessFileHandler(w http.ResponseWriter, r *http.Request) {
	// Allows only "POST" method.
	if r.Method != "POST" {
		http.Error(w, "Method is not supported.", http.StatusNotFound)
		return
	}

	fileAsString, err := getFileAsString(r)

	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	// Gets file content without next line characters.
	fileContent := utils.RemoveNextLines(fileAsString)

	// Validates file content has proper length for each entry.
	if math.Mod(float64(len(fileContent)), float64(utils.EntryCharsCount)) != 0 {
		http.Error(w, "Each entry must have at least 81 characters.", http.StatusBadRequest)
		return
	}

	// Number of entries that are into file.
	numberOfEntries := len(fileContent) / utils.EntryCharsCount

	// Save entries in string format.
	var stringEntries []string
	// Response that will be returned.
	var response []models.ProcessFileAPIResponse

	// Adds to stringEntries each of them.
	// e.g: [0] = fileContent[:81], [1] = fileContent[81:162], etc.
	for i := 0; i < numberOfEntries; i++ {
		stringEntries = append(stringEntries, fileContent[utils.EntryCharsCount*i:utils.EntryCharsCount*(i+1)])
	}

	// Process each stringEntries item to get his digits.
	for _, item := range stringEntries {
		var digits string

		for i := 0; i < 9; i++ {
			// Factor to move cursor and get full digit key.
			factor := 3 * i

			// Calculates the digit key.
			var digitKey string = string(item[0+factor]) +
				string(item[1+factor]) + string(item[2+factor]) +
				string(item[27+factor]) + string(item[28+factor]) +
				string(item[29+factor]) + string(item[54+factor]) +
				string(item[55+factor]) + string(item[56+factor])

			number, exists := utils.Digits[digitKey]

			// Validates if digitKey exists, if it exists add it to
			// digits, otherwise we add a "?".
			if exists {
				digits = digits + strconv.Itoa(number)
			} else {
				digits = digits + "?"
			}
		}

		// Adds digits to response.
		response = append(response, models.ProcessFileAPIResponse{
			Digits: digits,
			Status: utils.GetNumberStatus(digits),
		})
	}

	// Configures w to send response as JSON.
	w.Header().Set("Content-Type", "application/json")

	// Tries to encode response as json.
	if err := json.NewEncoder(w).Encode(response); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

// getFileAsString gets a file and parses it into a string.
func getFileAsString(r *http.Request) (string, error) {
	// Open buffer to put file content.
	var buf bytes.Buffer

	file, fileHeaders, err := r.FormFile("file")

	// Validates if user uploaded required file.
	if file == nil {
		return "", errors.New("file can't be empty")
	}

	// Validates if we have any err getting file.
	if err != nil {
		return "", err
	}

	fileName := fileHeaders.Filename
	fileNameSeparated := strings.Split(fileName, ".")
	fileExtension := fileNameSeparated[len(fileNameSeparated)-1]

	// Validates if file has ".txt" extension.
	if fileExtension != "txt" {
		return "", errors.New("only .txt files are allowed")
	}

	_, err = io.Copy(&buf, file)

	// Validates if file was copied to buffer successfully.
	if err != nil {
		return "", err
	}

	// Gets buffer content as string.
	contents := buf.String()

	// Reset current buffer.
	buf.Reset()

	return contents, nil
}
