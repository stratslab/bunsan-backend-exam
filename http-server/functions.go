package http_server

import (
	"fmt"
	"log"
	"net/http"
)

// RunServer runs main http server.
func RunServer() {
	// Gets serverPort from env file.
	serverPort := "8001"
	// APIs.
	http.HandleFunc("/process-file", ProcessFileHandler)

	fmt.Printf("Starting server at port: %v\n", serverPort)

	// Listens and serve http server using serverPort.
	if err := http.ListenAndServe(":"+serverPort, nil); err != nil {
		log.Fatal(err)
	}
}
