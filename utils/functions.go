package utils

import (
	"math"
	"regexp"
	"strconv"
	"strings"
)

// RemoveNextLines removes all next lines chars from a string.
func RemoveNextLines(text string) string {
	re := regexp.MustCompile(`\x{000D}\x{000A}|[\x{000A}\x{000B}\x{000C}\x{000D}\x{0085}\x{2028}\x{2029}]`)
	return re.ReplaceAllString(text, ``)
}

func GetNumberStatus(number string) string {
	if strings.Contains(number, "?") {
		return "ILL"
	}

	if ValidChecksum(number) {
		return "OK"
	} else {
		return "ERR"
	}
}

// ValidChecksum validates if a number has the proper checksum.
// e.g: 345882865 = (1*5)+(2*6)+(3*8)+(4*2)+(5*8)+(6*8)+(7*5)+(8*4)+(9*3)mod 11 = 0.
func ValidChecksum(number string) bool {
	if len(number) == 9 {
		sum := 0

		for i := 0; i < 9; i++ {
			sum += (i + 1) * ParseStringToInt(string(number[8-i]))
		}

		response := math.Mod(float64(sum), 11)

		return response == 0
	}

	return false
}

// ParseStringToInt parses a string number to int.
func ParseStringToInt(number string) int {
	intNumber, err := strconv.Atoi(number)

	if err != nil {
		panic("Error parsing number")
	}

	return intNumber
}
