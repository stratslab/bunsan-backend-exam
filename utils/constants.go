package utils

// Digits has all available digits/numbers, it allows get the actual number
// using the correct combination of chars.
var Digits = map[string]int{
	" _ | ||_|": 0,
	"     |  |": 1,
	" _  _||_ ": 2,
	" _  _| _|": 3,
	"   |_|  |": 4,
	" _ |_  _|": 5,
	" _ |_ |_|": 6,
	" _   |  |": 7,
	" _ |_||_|": 8,
	" _ |_| _|": 9,
}

// EntryCharsCount is the count of characters that every entry must have.
var EntryCharsCount int = 81
