package utils

import (
	"testing"
)

func TestValidChecksum(t *testing.T) {
	type test struct {
		input    string
		expected bool
	}

	tests := []test{
		test{
			input:    "345882865",
			expected: true,
		},
		test{
			input:    "457508000",
			expected: true,
		},
		test{
			input:    "664371495",
			expected: false,
		},
	}

	for _, x := range tests {
		v := ValidChecksum(x.input)
		if v != x.expected {
			t.Error("Expected", x.expected, "Got", v)
		}
	}
}
